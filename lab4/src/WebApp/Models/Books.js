const ServerError = require('./ServerError.js');

class BookPostBody {
    constructor(body) {

        this.name = body.name;
        this.author_id = body.author_id;

        if (this.name == null) {
            throw new ServerError("Book name is missing", 400);
        }

        if (this.author_id == null) {
            throw new ServerError("Author id is missing", 400);
        }
    }

    get Name() {
        return this.name;
    }

    get AuthorId() {
        return this.author_id;
    }
}

class BookPutBody extends BookPostBody {
    constructor(body, id) {
        super(body);
        this.id = parseInt(id);

        if (!this.id || this.id < 1) {
            throw new ServerError("Id should be a positive integer", 400);
        }
    }

    get Id() {
        return this.id;
    }
}

class BookPublisher {
    constructor(body, id) {
        this.id = parseInt(id);

        if (!this.id || this.id < 1) {
            throw new ServerError("Id should be a positive integer", 400);
        }

        this.price = body.price;
        this.publisher_id = parseInt(body.publisher_id);

        if (this.price == null) {
            throw new ServerError("Book price is missing", 400);
        }

        if (this.publisher_id == null) {
            throw new ServerError("Publisher id is missing", 400);
        }
    }

    get Id() {
        return this.id;
    }

    get Price() {
        return this.price;
    }

    get PublisherId() {
        return this.publisher_id;
    }
}

module.exports = {
    BookPostBody,
    BookPutBody,
    BookPublisher
}