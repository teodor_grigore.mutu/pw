const express = require('express');

const BookRepository = require("../../Infrastructure/PostgreSQL/Repository/BookRepository.js");
const PublisherRepository = require('../../Infrastructure/PostgreSQL/Repository/PublishersRepository');
const ServerError = require('../Models/ServerError.js');
const { BookPostBody, BookPutBody, BookPublisher } = require('../Models/Books.js')
const ResponseFilter = require('../Filters/ResponseFilter.js');

const Router = express.Router();

Router.get('/', async (req, res) => {

    const books = await BookRepository.getAllAsync();

    ResponseFilter.setResponseDetails(res, 200, books);
});


Router.get('/:id', async (req, res) => {

    let {
        id
    } = req.params;

    id = parseInt(id);

    if (!id || id < 1) {
        throw new ServerError("Id should be a positive integer", 400);
    }

    const book = await BookRepository.getByIdAsync(id);

    if (!book) {
        throw new ServerError(`Book with id ${id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 200, book);
});

Router.post('/', async (req, res) => {
    const bookBody = new BookPostBody(req.body);

    const book = await BookRepository.addBookAsync(bookBody.Name, bookBody.AuthorId);

    ResponseFilter.setResponseDetails(res, 201, book, req.originalUrl);
});

Router.delete('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    if (!id || id < 1) {
        throw new ServerError("Id should be a positive integer", 400);
    }

    const book = await BookRepository.deleteByIdAsync(parseInt(id));

    if (!book) {
        throw new ServerError(`Book with id ${id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 204, "Entity deleted succesfully");
});

Router.put('/:id', async (req, res) => {
    const booksPutBody = new BookPutBody(req.body, req.params.id);

    const book = await BookRepository.updateByIdAsync(booksPutBody.Id, booksPutBody.Name, booksPutBody.AuthorId);

    if (!book) {
        throw new ServerError(`Book with id ${id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 200, book);
});

Router.post('/:id/publishers', async (req, res) => {

    let {
        id
    } = req.params;

    id = parseInt(id);

    const publisherBook = new BookPublisher(req.body, id);
    if (!id || id < 1) {
        throw new ServerError("Id should be a positive integer", 400);
    }

    const book = await BookRepository.getByIdAsync(publisherBook.Id);

    if (!book) {
        throw new ServerError(`Book with id ${id} does not exist!`, 404);
    }

    const publisher = await PublisherRepository.getByIdAsync(publisherBook.PublisherId);

    if (!publisher) {
        res.send({ response: { message: `Publisher with id ${publisherBook.PublisherId} does not exist!`, errror: 'Not Found' } }).status(404);
        return;
    }

    const result = await BookRepository.addBookPublisher(publisherBook.Id, publisherBook.PublisherId, publisherBook.Price);

    ResponseFilter.setResponseDetails(res, 201, result, req.originalUrl);
});

Router.put('/:bookId/publishers/:publisherId', async (req, res) => {

    let {
        bookId,
        publisherId
    } = req.params;

    body = {
        'price': req.body.price,
        'publisher_id': publisherId
    };

    const publisherBook = new BookPublisher(body, bookId);
    const result = await BookRepository.updateBookPublisher(publisherBook.Id, publisherBook.PublisherId, publisherBook.Price);

    if (!result) {
        res.send({ response: { message: `Book with id ${bookId} and publisher id ${publisherId} does not exist!`, errror: 'Not Found' } }).status(404);
        return;
    }

    ResponseFilter.setResponseDetails(res, 200, result);
});

Router.delete('/:bookId/publishers/:publisherId', async (req, res) => {
    let {
        bookId,
        publisherId
    } = req.params;

    // to lazy so just use copy paste
    body = {
        'price': 10,
        'publisher_id': publisherId
    };

    const publisherBook = new BookPublisher(body, bookId);
    const result = await BookRepository.deleteBookPublisher(publisherBook.Id, publisherBook.PublisherId);

    if (!result) {
        res.send({ response: { message: `Book with id ${bookId} and publisher id ${publisherId} does not exist!`, errror: 'Not Found' } }).status(404);
        return;
    }

    ResponseFilter.setResponseDetails(res, 204, "Entity deleted succesfully");
});

module.exports = Router;