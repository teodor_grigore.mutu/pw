const Router = require('express')();

const AuthorsController = require('./AuthorsController.js');
const BooksController = require('./BookController');
const PublisherController = require('./PublishersController');


Router.use('/v1/authors', AuthorsController);
Router.use('/v1/books', BooksController)
Router.use('/v1/publishers', PublisherController);

module.exports = Router;