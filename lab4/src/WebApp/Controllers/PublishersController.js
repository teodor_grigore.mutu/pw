const express = require('express');
const ResponseFilter = require('../Filters/ResponseFilter.js');
const PublishersRepository = require("../../Infrastructure/PostgreSQL/Repository/PublishersRepository");
const ServerError = require('../Models/ServerError.js');
const { PublisherPostBody, PublisherPutBody } = require('../Models/Publishers');

const Router = express.Router();

Router.get('/', async (req, res) => {

    const publishers = await PublishersRepository.getAllAsync();

    ResponseFilter.setResponseDetails(res, 200, publishers);
});

Router.get('/:id', async (req, res) => {

    let {
        id
    } = req.params;

    id = parseInt(id);

    if (!id || id < 1) {
        throw new ServerError("Id should be a positive integer", 400);
    }

    const publishers = await PublishersRepository.getByIdAsync(id);

    if (!publishers) {
        res.send({ response: { message: `Publisher with ${id} does not exists!`, error: 'Not Found' } }).status(404);
        return
    }

    ResponseFilter.setResponseDetails(res, 200, publishers);
});

Router.post('/', async (req, res) => {
    const publisherBody = new PublisherPostBody(req.body);

    const publisher = await PublishersRepository.addAsync(publisherBody.Name);

    ResponseFilter.setResponseDetails(res, 201, publisher, req.originalUrl);
});

Router.delete('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    if (!id || id < 1) {
        throw new ServerError("Id should be a positive integer", 400);
    }

    const publisher = await PublishersRepository.deleteByIdAsync(parseInt(id));

    if (!publisher) {
        throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 204, "Entity deleted succesfully");
});

Router.put('/:id', async (req, res) => {
    const publisherPutBody = new PublisherPutBody(req.body, req.params.id);

    const publisher = await PublishersRepository.updateByIdAsync(publisherPutBody.Id, publisherPutBody.name);

    if (!publisher) {
        throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 200, publisher);
});

module.exports = Router;
