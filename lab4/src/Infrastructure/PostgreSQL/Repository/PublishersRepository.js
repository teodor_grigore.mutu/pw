const {
    queryAsync
} = require('..');

const BookRepository = require('./BookRepository');

const addAsync = async (name) => {
    console.info(`Adding publisher in database async...`);

    const publishers = await queryAsync('INSERT INTO publishers (name) VALUES ($1) RETURNING *', [name]);
    return publishers[0];
};

const getAllAsync = async () => {
    console.info(`Getting all publishers from database async...`);

    return await queryAsync('SELECT * FROM publishers');
};

const getByIdAsync = async (id) => {
    console.info(`Getting the publisher with id ${id} from database async...`);

    const publishers = await queryAsync('SELECT * FROM publishers WHERE id = $1', [id]);
    if (publishers.length === 0) {
        return null;
    }

    publishers[0]["books"] = [];
    const publisher_books = await queryAsync('SELECT * FROM publishers_books WHERE publisher_id = $1', [id]);

    for (let book of publisher_books) {
        let bookInfo = await BookRepository.getByIdAsync(book["book_id"]);

        bookInfo["price"] = book["price"];

        publishers[0]["books"].push(bookInfo);
    }
    return publishers[0];
};

const updateByIdAsync = async (id, name) => {
    console.info(`Updating the publisher with id ${id} from database async...`);

    const publishers = await queryAsync('UPDATE publishers SET name = $1 WHERE id = $2 RETURNING *', [name, id]);
    return publishers[0];
};

const deleteByIdAsync = async (id) => {
    console.info(`Deleting the author with id ${id} from database async...`);

    const publishers = await queryAsync('DELETE FROM publishers WHERE id = $1 RETURNING *', [id]);
    return publishers[0];

};

module.exports = {
    addAsync,
    getAllAsync,
    getByIdAsync,
    updateByIdAsync,
    deleteByIdAsync
}