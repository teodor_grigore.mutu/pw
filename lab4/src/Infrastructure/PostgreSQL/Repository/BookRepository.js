const {
    queryAsync
} = require('..');

const addBookAsync = async (name, author_id) => {
    console.info(`Adding book in database async...`);

    const book = await queryAsync('INSERT INTO books (name, author_id) VALUES ($1, $2) RETURNING *', [name, author_id]);
    return book[0];
};

const getAllAsync = async () => {
    console.info(`Getting all books from database async...`);

    return await queryAsync('SELECT * FROM books');
};

const getByIdAsync = async (id) => {
    console.info(`Getting the book with id ${id} from database async...`);

    const books = await queryAsync('SELECT * FROM books INNER JOIN authors ON books.author_id = authors.id WHERE books.id = $1', [id]);
    books[0]['id'] = id;
    return books[0];
};

const deleteByIdAsync = async (id) => {
    console.info(`Deleting the book with id ${id} from database async...`);

    const books = await queryAsync('DELETE FROM books WHERE id = $1 RETURNING *', [id]);
    return books[0];
}

const updateByIdAsync = async (id, name, author_id) => {
    console.info(`Updating the books with id ${id} from database async...`);

    const books = await queryAsync('UPDATE books SET name = $1, author_id = $2 WHERE id = $3 RETURNING *', [name, author_id, id]);
    return books[0];
};

const addBookPublisher = async (book_id, publisher_id, price) => {
    console.info(`Adding book to publisher in database async...`);

    const bookPublisher = await queryAsync('INSERT INTO publishers_books (book_id, publisher_id, price) VALUES ($1, $2, $3) RETURNING *', [book_id, publisher_id, price]);
    return bookPublisher[0];
}

const updateBookPublisher = async (book_id, publisher_id, price) => {
    console.info(`Updating the books publisher with book_id ${book_id} and publisher_id ${publisher_id} from database async...`);

    const bookPublisher = await queryAsync('UPDATE publishers_books SET price = $1 WHERE book_id = $2 and publisher_id = $3 RETURNING *', [price, book_id, publisher_id]);
    return bookPublisher[0];
}

const deleteBookPublisher = async (book_id, publisher_id) => {
    console.info(`Deleting the book with id ${book_id} and publisher_id ${publisher_id} from database async...`);
    const result = await queryAsync('DELETE FROM publishers_books WHERE book_id = $1 and publisher_id = $2 RETURNING *', [book_id, publisher_id]);
    return result[0];
}

module.exports = {
    addBookAsync,
    getAllAsync,
    getByIdAsync,
    deleteByIdAsync,
    updateByIdAsync,
    addBookPublisher,
    updateBookPublisher,
    deleteBookPublisher
}