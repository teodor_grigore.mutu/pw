import React, { useReducer } from 'react';
import styles from './Counter.module.css';
import { ReactComponent as Logo } from './logo.svg';

const initialState = { count: 0, images: [] };
function reducer(state, action) {
  switch (action.type) {
    case 'increment':
      return { count: state.count + 1, images: [...state.images, state.count + 1] };
    case 'decrement':
      state.images.pop();
      const images_after_remove = state.images;
      return { count: state.count - 1, images: [...images_after_remove] };
    case 'reset':
      return { count: 0, images: [] };
    default:
      throw new Error();
  }
};

const Counter = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  // THIS IS PURE GARBAGE NEVER WRITING IN THIS PEPEGA FRAMEWORK EVER

  const listItems = state.images.map((entry) => <li key={entry}>
    < Logo width="20px" height="20px" />
  </li>)

  return (
    <>
      <div className={styles.Counter}>
        <div className={styles.display_border}>
          <ul>
            {listItems}
          </ul>
        </div>
        <div className={styles.button_display}>
          <div className={styles.row}>
            <div>
              <button onClick={() => dispatch({ type: 'increment' })}> Increment </button>
            </div>
            <div>
              <button onClick={() => dispatch({ type: 'decrement' })}> Decrement </button>
            </div>
            <div>
              <button onClick={() => dispatch({ type: 'reset' })}> Reset </button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
};

export default Counter;
