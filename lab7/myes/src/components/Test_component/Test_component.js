import React from 'react';
import PropTypes from 'prop-types';
import styles from './Test_component.module.css';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Counter from '../Counter/Counter';

const Test_component = () => (
  <div className={styles.Test_component}>
    <div className={styles.content}>
      <div>
        <Header />
      </div>
      <div style={{
        position: 'absolute', left: '50%', top: '50%',
        transform: 'translate(-50%, -50%)'
      }}>
        <Counter />
      </div>
    </div>

    <Footer />
  </div>
);

Test_component.propTypes = {};

Test_component.defaultProps = {};

export default Test_component;
