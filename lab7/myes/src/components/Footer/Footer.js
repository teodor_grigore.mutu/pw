import React from 'react';
import PropTypes from 'prop-types';
import styles from './Footer.module.css';

const Footer = () => (
  <div className={[styles.Footer, styles.border].join(' ')}>
    <div className={styles.text_color}>
      <p> UPB </p>
    </div>
    <div className={styles.text_color}>
      <p> Laborator #2 PWEB</p>
    </div>
  </div>
);

Footer.propTypes = {};

Footer.defaultProps = {};

export default Footer;
