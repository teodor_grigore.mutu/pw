import React from 'react';
import PropTypes from 'prop-types';
import styles from './Header.module.css';

const Header = () => (
  <div className={[styles.Header, styles.border, styles.bottom].join(' ')}>
    <div>
      <p>UPB</p>
    </div>
    <div>
      <p>Counter</p>
    </div>
  </div>
);

Header.propTypes = {};

Header.defaultProps = {};

export default Header;
