import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ShowRoleData } from '../role/role.component';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-show-role',
  templateUrl: './show-role.component.html',
  styleUrls: ['./show-role.component.scss']
})
export class ShowRoleComponent implements OnInit {

  visible = false;
  selectable = false;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  permissionForm = new FormControl();
  filteredPermissions: Observable<string[]>;
  permissions: string[] = [];
  allPermissions: string[] = ["create-user", "delete-user", "list-user", "modify-user",
    "create-role", "list-role", "delete-role", "modify-role",
    "create-organization", "delete-organization", "modify-organization", "list-organization",];

  roleName: string;

  @ViewChild('permissionInput')
  permissionInput!: ElementRef<HTMLInputElement>;
  @ViewChild('auto')
  matAutocomplete!: MatAutocomplete;

  constructor(
      private roleService: RoleService,
      public dialogRef: MatDialogRef<ShowRoleComponent>,
      private notificationService: NotificationsService,
      @Inject(MAT_DIALOG_DATA)public data: ShowRoleData
    ) { 
      this.filteredPermissions = this.permissionForm.valueChanges.pipe(
        startWith(null),
        map((permission: string | null) => permission ? this._filter(permission) : this.allPermissions.slice()));

      this.data.data.permissions.map((x) => {
        this.permissions.push(x.permission);
        let index = this.allPermissions.indexOf(x.permission);
        this.allPermissions.splice(index, 1);
      });

      this.roleName = data.data.name;
    }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allPermissions.filter(permission => permission.toLowerCase().indexOf(filterValue) === 0);
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    if (value) {
      this.permissions.push(value);
    }

    this.permissionForm.setValue(null);
  }

  remove(permission: string): void {
    const index = this.permissions.indexOf(permission);

    if (index >= 0) {
      this.permissions.splice(index, 1);
      this.allPermissions.push(permission);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.permissions.push(event.option.viewValue);
    const index = this.allPermissions.indexOf(event.option.viewValue);
    this.allPermissions.splice(index, 1);
    this.permissionInput.nativeElement.value = '';
    this.permissionForm.setValue(null);
  }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

  updateRole() {
    this.roleService.updateRole(this.data.data.id, this.roleName, this.permissions).subscribe(
      (res) => {
        this.notificationService.success('Success', `Role with id ${this.data.data.id} update`);
        this.dialogRef.close(res);
      }, (err) => {
        this.notificationService.error('Error', err.error.message);
      }
    )
  }
}
