import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatPasswordStrengthComponent } from '@angular-material-extensions/password-strength';
import { AbstractControl, FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { UserCredentials } from '../interfaces/user-credentials.interface';
import { AuthService } from '../services/auth.service';
import { RequestResponse } from '../interfaces/request.interface';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterComponent implements OnInit {

  hide = true;
  showDetails = true;
  registerForm: FormGroup;
  matcher = new MyErrorStateMatcher();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private notificationService: NotificationsService,
    private router: Router
  ) {

    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    }, { validator: this.checkPasswords });

  }

  ngOnInit(): void {
  }

  checkPasswords(group: FormGroup): null | {} {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }

  onSubmit(): void {

    const email = this.email?.value;
    const password = this.password?.value;

    const userAuth = new UserCredentials(email, password);
    this.authService.register(userAuth).subscribe((result: RequestResponse) => {
      this.notificationService.success('Succes', result.message);
      this.router.navigate(['/login']);
    }, (error) => {
      this.notificationService.error('Failed', error.error.message);
    });
  }

  get email(): null | AbstractControl {
    return this.registerForm.get('email');
  }

  get password(): null | AbstractControl {
    return this.registerForm.get('password');
  }

  get confirmPassword(): null | AbstractControl {
    return this.registerForm.get('confirmPassword');
  }

}
