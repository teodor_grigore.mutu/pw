import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {
  
  baseUrl = environment.baseUrl + '/organization/';

  constructor(private http: HttpClient) { }


  getOrganizationName() {
    return this.http.get<{name: string}>(this.baseUrl + 'name');
  }

  createOrganization(name: string) {
    return this.http.post(this.baseUrl + 'create', { name });
  }
}
