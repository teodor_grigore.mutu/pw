import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Userconfirmation, UserCredentials } from '../interfaces/user-credentials.interface';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserData } from '../user/user.component';

interface AuthResponse {
  accessToken: string;
  refreshToken: string;
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token: BehaviorSubject<string | any>;
  private refreshKey: BehaviorSubject<string | any>;
  private loggedIn: BehaviorSubject<boolean>;
  baseUrl = environment.baseUrl + '/user/';

  public organizationName: string = 'Not defined';

  constructor(private http: HttpClient, private router: Router) {
    this.token = new BehaviorSubject<string | any>(localStorage.getItem('token'));
    this.refreshKey = new BehaviorSubject<string | any>(localStorage.getItem('refreshToken'));
    this.loggedIn = new BehaviorSubject<boolean>(false);
  }

  public get currentTokenValue(): string {
    return this.token.value;
  }

  public get is_logged_in(): boolean {
    return localStorage.getItem('token') !== null;
  }

  register(userCredentials: UserCredentials): Observable<any> {
    return this.http.post(this.baseUrl + 'register', userCredentials);
  }

  confirmation(userConnfirmation: Userconfirmation): Observable<any> {
    return this.http.post(this.baseUrl + 'confirmation', userConnfirmation);
  }

  private refreshTokenTimeout: any;

  private setup_storage(result: AuthResponse) {
    localStorage.setItem('token', result.accessToken);
    localStorage.setItem('refreshToken', result.refreshToken);
    this.token.next(result.accessToken);
    this.refreshKey.next(result.refreshToken);
  }

  authenticate(userCredentials: UserCredentials): Observable<any> {
    return this.http.post<AuthResponse>(this.baseUrl + 'auth', userCredentials).pipe(
      map((result: AuthResponse) => {
        this.setup_storage(result);
        this.startRefreshTokenTimer();
        this.loggedIn.next(true);
        return result;
      })
    );
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
    this.token.next(null);
    this.refreshKey.next(null);
    this.loggedIn.next(false);
    this.stopRefreshTokenTimer();
    this.router.navigate(['/login']);
  }

  resend(userConfirmation: Userconfirmation): Observable<any> {
    return this.http.post(this.baseUrl + 'resend', userConfirmation);
  }

  refresh(): Observable<any> {
    return this.http.post<AuthResponse>(this.baseUrl + 'refresh', { refreshToken: this.refreshKey.value });
  }

  private startRefreshTokenTimer(): void {
    const jwtToken = JSON.parse(atob(this.currentTokenValue.split('.')[1]));
  
    const expires = new Date(jwtToken.exp * 1000);
    const timeout = expires.getTime() - Date.now() - (60 * 1000);

    this.refreshTokenTimeout = setTimeout(() => {
      this.refresh().subscribe(
        (result: AuthResponse) => {
          this.setup_storage(result);
        }
      );
    }, timeout);
  }

  private stopRefreshTokenTimer(): void {
    clearTimeout(this.refreshTokenTimeout);
  }

  createUser(email: string, roleId: number) {
    return this.http.post(this.baseUrl + 'create', { email: email, roleId: roleId});
  }

  getUsers() {
    return this.http.get<UserData[]>(this.baseUrl);
  }

  deleteUser(id: number) {
    return this.http.delete(this.baseUrl + `${id}`);
  }

  updateUser(id: number, roleId: number) {
    return this.http.put(this.baseUrl + `${id}`, { roleId: roleId });
  }
}
