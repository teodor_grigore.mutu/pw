import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RoleData } from '../role/role.component';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  baseUrl = environment.baseUrl + '/role';

  constructor(private http: HttpClient) { }


  createRole(name: string, permission: string[]) {
    return this.http.post(this.baseUrl + '/create', { name: name, permissions: permission });
  }

  getRoles() {
    return this.http.get<RoleData[]>(this.baseUrl);
  }

  deleteRole(roleId: number) {
    return this.http.delete(this.baseUrl + `/${roleId}`);
  }

  updateRole(roleId: number, name: string, permission: string[]) {
    return this.http.put<RoleData>(this.baseUrl + `/${roleId}`, { id: roleId, name: name, permissions: permission })
  }
}
