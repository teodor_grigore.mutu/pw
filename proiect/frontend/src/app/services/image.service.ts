import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  baseUrl = environment.baseUrl + '/user/';

  constructor(private http: HttpClient) { }

  getProfileImage(): Observable<Blob> {
    return this.http.get(this.baseUrl + 'profile-image', { responseType: 'blob' });
  }
}
