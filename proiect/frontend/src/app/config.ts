export const notificationOptions: {
    position: any,
    timeOut: number,
    showProgressBar: boolean,
    pauseOnHover: boolean,
    clickToClose: boolean,
    clickIconToClose: boolean,
    animate: any,
} = {
    position: ['top', 'right'],
    timeOut: 5000,
    showProgressBar: true,
    pauseOnHover: true,
    clickToClose: true,
    clickIconToClose: true,
    animate: 'fromLeft'
}