import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { RequestResponse } from '../interfaces/request.interface';
import { UserCredentials } from '../interfaces/user-credentials.interface';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private notificationService: NotificationsService
  ) {
    this.loginForm = this.formBuilder.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]],
      }
    );
  }

  ngOnInit(): void {
  }

  get email(): null | AbstractControl {
    return this.loginForm.get('email');
  }

  get password(): null | AbstractControl {
    return this.loginForm.get('password');
  }

  onSubmit(): void {
    const email = this.email?.value;
    const password = this.password?.value;
    const userAuth = new UserCredentials(email, password);

    this.authService.authenticate(userAuth).subscribe(
      result => {
        this.notificationService.success('Logged in succesfully!');
        this.router.navigate(['/home'])
      },
      error => {
        this.notificationService.error('Failed', error.error.message);
      });
  }

}
