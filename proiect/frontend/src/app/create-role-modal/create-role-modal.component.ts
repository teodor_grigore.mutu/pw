import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef } from '@angular/material/dialog';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-create-role-modal',
  templateUrl: './create-role-modal.component.html',
  styleUrls: ['./create-role-modal.component.scss']
})
export class CreateRoleModalComponent implements OnInit {

  visible = false;
  selectable = false;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  permissionForm = new FormControl();
  filteredPermissions: Observable<string[]>;
  permissions: string[] = [];
  allPermissions: string[] = ["create-user", "delete-user", "list-user", "modify-user",
    "create-role", "list-role", "delete-role", "modify-role",
    "create-organization", "delete-organization", "modify-organization", "list-organization",];

  @ViewChild('permissionInput')
  permissionInput!: ElementRef<HTMLInputElement>;
  @ViewChild('auto')
  matAutocomplete!: MatAutocomplete;

  roleName: any;

  constructor(public dialogRef: MatDialogRef<CreateRoleModalComponent>,
    private notificationService: NotificationsService,
    private roleService: RoleService
    ) {
    this.filteredPermissions = this.permissionForm.valueChanges.pipe(
      startWith(null),
      map((permission: string | null) => permission ? this._filter(permission) : this.allPermissions.slice()));
  }
  ngOnInit(): void {}

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    if (value) {
      this.permissions.push(value);
    }

    this.permissionForm.setValue(null);
  }

  remove(permission: string): void {
    const index = this.permissions.indexOf(permission);

    if (index >= 0) {
      this.permissions.splice(index, 1);
      this.allPermissions.push(permission);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.permissions.push(event.option.viewValue);
    const index = this.allPermissions.indexOf(event.option.viewValue);
    this.allPermissions.splice(index, 1);
    this.permissionInput.nativeElement.value = '';
    this.permissionForm.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allPermissions.filter(permission => permission.toLowerCase().indexOf(filterValue) === 0);
  }


  createRole() {
    this.roleService.createRole(this.roleName, this.permissions).subscribe(
      (res) => {
        this.notificationService.success('Success', `Role with name ${this.roleName} created!`);
        this.close()
      },
      (err) => {
        this.notificationService.error('Error', err.error.message);
      }
    )
  }

  close() {
    this.dialogRef.close();
  }
}
