export class UserCredentials {
    email: string;

    password: string;

    constructor(email: string, password: string) {
        this.email = email;
        this.password = password;
    }
}

export class Userconfirmation {
    email: string;

    token: string;

    constructor(email: string, token: string) {
        this.email = email;
        this.token = token;
    }
}
