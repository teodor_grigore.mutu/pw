import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationsService } from 'angular2-notifications';
import { CreateUserModalComponent } from '../create-user-modal/create-user-modal.component';
import { RoleData } from '../role/role.component';
import { AuthService } from '../services/auth.service';
import { RoleService } from '../services/role.service';
import { UserData } from '../user/user.component';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {

  roles: RoleData[] = [];
  role = new FormControl(null, [Validators.required]);
  constructor(
    private notificationService: NotificationsService,
    public dialogRef: MatDialogRef<CreateUserModalComponent>,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: UserData,
    private roleService: RoleService,) { }

  
  ngOnInit(): void {
    this.roleService.getRoles().subscribe((res) => {
      this.roles = res;
    });
  }


  updateUser(): void {
    this.authService.updateUser(this.data.id, this.role.value).subscribe(
      (res) => {
        this.notificationService.success('Success', 'User updated!')
        this.dialogRef.close();
      }, (err) => {
        this.notificationService.error('Error', err.error.message);
      }
    )
  }
}
