import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationsService } from 'angular2-notifications';
import { OrganizationService } from '../services/organization.service';

@Component({
  selector: 'app-create-organization-modal',
  templateUrl: './create-organization-modal.component.html',
  styleUrls: ['./create-organization-modal.component.scss']
})
export class CreateOrganizationModalComponent implements OnInit {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private organizationService: OrganizationService,
    private notificationService: NotificationsService,
    public dialogRef: MatDialogRef<CreateOrganizationModalComponent>,
    @Inject(MAT_DIALOG_DATA) public email: string,
  ) { 
    this.form = this.formBuilder.group({
      name: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.close();
  }

  get name() {
    return this.form.get("name");
  }

  save() {
    this.organizationService.createOrganization(this.name?.value).subscribe(
      (res) => {
        this.notificationService.success('Success', `Organization with name ${this.name?.value} create!`);
        this.close();
      }, (error) => {
        this.notificationService.error('Error', error.error.message);
      }
    )
  }

  close() {
    this.dialogRef.close();
  }

}
