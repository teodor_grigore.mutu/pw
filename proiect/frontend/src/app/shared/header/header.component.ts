import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { OrganizationService } from 'src/app/services/organization.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();

  organizationName: string = 'Not defined';
  constructor(private organizationService: OrganizationService, public authService: AuthService) { }

  
  ngOnInit(): void {
    
    this.organizationService.getOrganizationName().subscribe((res) => {
        if (res.name) {
          this.organizationName = res.name;
        }
    });
  
  }

  toggleSideBar() {
    this.toggleSideBarForMe.emit();
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }



}
