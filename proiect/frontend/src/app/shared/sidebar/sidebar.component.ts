import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ImageService } from 'src/app/services/image.service';
import jwt_decode from 'jwt-decode';
import { MatDialog } from '@angular/material/dialog';
import { CreateOrganizationModalComponent } from 'src/app/create-organization-modal/create-organization-modal.component';
import { CreateRoleModalComponent } from 'src/app/create-role-modal/create-role-modal.component';
import { CreateUserModalComponent } from 'src/app/create-user-modal/create-user-modal.component';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  panelOpenState = false;
  icon = true;
  
  image: any;
  user: any;
  userItems: {
    'route': string,
    'icon': string,
    'message': string
  }[] = [
    {
      route: '/home/users',
      icon: 'format_list_bulleted',
      message: 'List users',
    }
  ];
  
  roleItems: {
    'route': string,
    'icon': string,
    'message': string
  }[] = [
      {
        route: '/home/roles',
        icon: 'format_list_bulleted',
        message: 'List roles',
      }
  ];

  resourceItems: {
    'route': string,
    'icon': string,
    'message': string
  }[] = [
      {
        route: '/create-resource',
        icon: 'create',
        message: 'Create resource',
      },
      {
        route: '/home/resources',
        icon: 'format_list_bulleted',
        message: 'List resources',
      }
  ];

  constructor(private imageService: ImageService, private authService: AuthService,
    public dialog: MatDialog) { }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.image = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  ngOnInit(): void {
    this.imageService.getProfileImage().subscribe((res) => {
      this.createImageFromBlob(res);
    });

    try {
      this.user = jwt_decode(this.authService.currentTokenValue);
    }
    catch (error) {
      
    }
  }

  openDialog(): void {
    this.dialog.open(CreateOrganizationModalComponent, {
      data: this.user.email,
    });
  }

  openRoleDialog(): void {
    this.dialog.open(CreateRoleModalComponent, { });
  }

  openUserDialog(): void{
    this.dialog.open(CreateUserModalComponent, { });
  }
}

