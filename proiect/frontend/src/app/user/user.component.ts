import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from '../services/auth.service';
import { UserUpdateComponent } from '../user-update/user-update.component';


export interface UserData {
  id: number;
  email: string;
  role: {
    id: number,
    name: string,
  };
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  displayedColumns: string[] = ['email', 'role','actions'];
  dataSource: MatTableDataSource<UserData> = new MatTableDataSource<UserData>();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

  constructor(private authService: AuthService,
      private notificationService: NotificationsService,
      public dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.authService.getUsers().subscribe((res) => {
      this.dataSource = new MatTableDataSource(res);
    }, err => {
      this.notificationService.error('Error', err.error.message)
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  editUser(row: UserData): void {
    this.dialog.open(UserUpdateComponent, {
      data: row
    })
  }

  deleteUser(row: UserData): void {
    this.authService.deleteUser(row.id).subscribe(
      (res) => {
        this.notificationService.success('Success', 'User deleted!');
        let data = this.dataSource.data;
        const index = data.indexOf(row);
        data.splice(index, 1)
        this.dataSource = new MatTableDataSource(data);
      }, (err) => {
        this.notificationService.error('Error', err.error.message);
      }
    )
  }
}
