import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { LoadingService } from '../services/loading.service';
import { finalize } from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    public loadingService: LoadingService
    ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // this.loadingService.isLoading.next(true);
    if (this.authService.is_logged_in) {
      request = request.clone({
        setHeaders: { Authorization: `Bearer ${this.authService.currentTokenValue}` }
      });
    }

    return next.handle(request).pipe(
      finalize(() => {
        this.loadingService.isLoading.next(false);
      })
    );
  }
}
