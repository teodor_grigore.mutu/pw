import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { NotificationsService } from 'angular2-notifications';
import { RoleData } from '../role/role.component';
import { AuthService } from '../services/auth.service';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-create-user-modal',
  templateUrl: './create-user-modal.component.html',
  styleUrls: ['./create-user-modal.component.scss']
})
export class CreateUserModalComponent implements OnInit {

  email = new FormControl('', [Validators.email, Validators.required]);

  form: FormGroup;
  selectedRole: any;
  roles: RoleData[] = [];
  role = new FormControl(null, [Validators.required]);

  constructor(
      private formBuilder: FormBuilder,
      private roleService: RoleService,
      private authService: AuthService,
      private notificationService: NotificationsService,
      public dialogRef: MatDialogRef<CreateUserModalComponent>,
    ) { 
    this.form = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }


  ngOnInit(): void {
    this.roleService.getRoles().subscribe((res) => {
      this.roles = res;
    });
  }

  createUser() {
    this.authService.createUser(this.form.get('email')?.value, this.role.value).subscribe(
      (res) => {
        this.notificationService.success('Success', 'User created!');
        this.dialogRef.close();
      }, (err) => {
        this.notificationService.error('Error', err.error.message);
      }
    )
  }
}
