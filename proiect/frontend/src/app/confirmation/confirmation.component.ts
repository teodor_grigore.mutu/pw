import { AfterViewInit, Component, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { RequestResponse } from '../interfaces/request.interface';
import { Userconfirmation, UserCredentials } from '../interfaces/user-credentials.interface';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  token: any;
  email: any;

  notificationSettings = {
    timeOut: 0,
    showProgressBar: false,
    pauseOnHover: false,
    clickToClose: true,
    clickIconToClose: true,
  };

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private notificationService: NotificationsService,
    private router: Router) {

    this.token = this.route.snapshot.queryParams.token;
    this.email = this.route.snapshot.queryParams.email;

  }
  ngOnInit(): void {
    this.confirmEmail();
  }

  private confirmEmail(): void {

    const userConfirmation = new Userconfirmation(this.email, this.token);
    this.authService.confirmation(userConfirmation).subscribe({
      next: (result) => {
        const notification = this.notificationService.success(
          result.message,
          'Close the notification to go to login',
          this.notificationSettings);

        notification.clickIcon?.subscribe(_ => {
          this._redirectToLogin(this.router, null, null, null);
        });

        notification.click?.subscribe(_ => {
          this._redirectToLogin(this.router, null, null, null);
        });
      },
      error: (error) => {

        let func = this._resendConfirmation;
        let message = 'Close the notification to resend confirmation email';

        if (error.error.message === `User with email ${this.email} is verified`) {
          func = this._redirectToLogin;
          message = 'Close the notification to go to login';
        }

        const notification = this.notificationService.error(
          error.error.message,
          message,
          this.notificationSettings);

        notification.click?.subscribe(_ => {
          func(this.router, userConfirmation, this.authService, this.notificationService);
        });
        notification.clickIcon?.subscribe(_ => {
          func(this.router, userConfirmation, this.authService, this.notificationService);
        });
      }
    });
  }

  private _redirectToLogin(router: Router, userConfirmation: any, x: any, y: any): void {
    router.navigate(['/login']);
  }

  private _resendConfirmation(
    router: Router,
    userConfirmation: Userconfirmation,
    authService: AuthService,
    notificationService: NotificationsService): void {
    console.log(userConfirmation);
    authService.resend(userConfirmation).subscribe(
      {
        next: (result) => {
          notificationService.success(result.message);
        },
        error: (error) => {
          notificationService.error(error.error.message);
        }
      }
    );
  }
}
