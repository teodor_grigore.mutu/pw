import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NotificationsService } from 'angular2-notifications';
import { RoleService } from '../services/role.service';
import { ShowRoleComponent } from '../show-role/show-role.component';

export interface RoleData {
  id: number;
  name: string;
  permissions: {
    id: number,
    permission: string
  }[];
}

export interface ShowRoleData {
  edit: boolean;
  data: RoleData;
}

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

  displayedColumns: string[] = ['name', 'actions'];
  dataSource: MatTableDataSource<RoleData> = new MatTableDataSource<RoleData>();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

  constructor(
      private roleService: RoleService, 
      private notificationService: NotificationsService,
      public dialog: MatDialog
    ) {
    this.roleService.getRoles().subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
    }, err => {
      this.notificationService.error('Error', err.error.message);
    })
  }
  ngOnInit(): void { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getRole(row: RoleData) {
    this.dialog.open(ShowRoleComponent, {
      data: {edit: false, data: row}
    })
  }

  deleteRole(row: RoleData) {
    this.roleService.deleteRole(row.id).subscribe(
      (res) => {
        this.notificationService.success('Success', 'Role deleted!');
        let data = this.dataSource.data;
        const index = data.indexOf(row);
        data.splice(index, 1)
        this.dataSource = new MatTableDataSource(data);
      }, (err) => {
        this.notificationService.error('Error', err.error.message);
      }
    );
  }

  editRole(row: RoleData) {
    const dialogRef = this.dialog.open(ShowRoleComponent, {
      data: { edit: true, data: row }
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let data = this.dataSource.data;
        const index = data.findIndex(r => r.id === result.id);
        data[index] = result;
        this.dataSource = new MatTableDataSource(data);
      }
    })
  }
}
