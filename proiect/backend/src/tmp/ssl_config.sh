#!/bin/bash

# CA key
openssl genrsa -out rootCA.key 4096

# CA root cert
openssl req -x509 -new -nodes -key rootCA.key \
    -subj "/C=RO/ST=Bucharest/O=Paddy/OU=R&D/CN=backend.api.com" \
    -sha256 -days 1024 -out rootCA.crt

# Cert key
openssl genrsa -out localhost.key 2048

# Create csr
openssl req -new -sha256 \
    -key localhost.key \
    -subj "/C=RO/ST=Bucharest/O=Paddy/OU=R&D/CN=backend.api.com" \
    -reqexts SAN \
    -config <(cat /etc/ssl/openssl.cnf <(printf "\n[SAN]\nsubjectAltName=DNS:backend.api.com")) \
    -out localhost.csr

# Create the crt
openssl x509 -req -extfile <(printf "subjectAltName=DNS:www.backend.api.com,DNS:localhost")\
    -in localhost.csr -CA rootCA.crt -CAkey rootCA.key \
    -CAcreateserial -out localhost.crt \
    -days 500 -sha256

# remove previous rootCA
sudo rm -rf /usr/local/share/ca-certificates/backend
sudo update-ca-certificates -f

# Add ca to trusted
sudo mkdir -p /usr/local/share/ca-certificates/backend
sudo chmod 755 /usr/local/share/ca-certificates/backend
sudo cp rootCA.crt /usr/local/share/ca-certificates/backend
sudo chmod 644 /usr/local/share/ca-certificates/backend/rootCA.crt
sudo update-ca-certificates

# compose chain
cat localhost.crt rootCA.crt >> localhost.chained.crt
