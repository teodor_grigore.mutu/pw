require('dotenv').config()
import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailService {

    constructor(private readonly mailerService: MailerService) { }

    public async sendConfirmationEmail(userEmail: string, userToken: string) {
        return this
            .mailerService
            .sendMail({
                to: userEmail,
                from: process.env.EMAIL_ID,
                subject: 'Account confirmation ✔',
                template: 'index',
                context: {
                    urlParam: userToken,
                },
            });
    }

    public async createUserAcoount(email: string, password: string) {
        return await this.mailerService.sendMail({
            to: email,
            from: process.env.EMAIL_ID,
            subject: 'Account created ✔',
            template: 'user',
            context: {
                email: email,
                password: password
            }
        });
    }

}
