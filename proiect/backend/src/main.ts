import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as fs from 'fs';

async function bootstrap() {
  const httpsOptions = {
    // key: fs.readFileSync('./src/tmp/localhost.key'),
    // cert: fs.readFileSync('./src/tmp/localhost.chained.crt'),
  };
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  await app.listen(3000);
}
bootstrap();
