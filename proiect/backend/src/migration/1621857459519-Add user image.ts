import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUserImage1621857459519 implements MigrationInterface {
    name = 'AddUserImage1621857459519'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "imagePath" text NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "imagePath"`);
    }

}
