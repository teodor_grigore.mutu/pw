require('dotenv').config()
import { TypeOrmModuleOptions } from '@nestjs/typeorm'

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: ['./dist/**/entities/*.entity.{js,ts}'],
    migrationsTableName: 'migration',

    migrations: ['./dist/**/migration/*.{js,ts}'],

    cli: {
        migrationsDir: './src/migration',
    },
    migrationsRun: true,
    synchronize: false,
};
