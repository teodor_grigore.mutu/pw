import { ConflictException, InternalServerErrorException } from "@nestjs/common";
import { UserDto, UserLoginDto } from "src/dto/user.dto";
import { User } from "src/entities/user.entity";
import { EntityRepository, Repository } from "typeorm";
import * as bcrypt from "bcrypt";
import { RoleRepository } from "./role.repository";
import { MailService } from "src/services/mail/mail.service";
import { v4 as uuidv4 } from 'uuid';
const font = __dirname + '/font.ttf';

const materialColors = ['d32f2f', 'C2185B', '7B1FA2', '512DA8', '303F9F', '1976D2', '0097A7', '00796B', '388E3C', '689F38', 'AFB42B', 'FBC02D', 'FFA000', 'F57C00', 'E64A19', '5D4037', '616161', '455A64', 'DB9BB4', '7E4A88'];
const gm = require('gm');

 export const createImage = (email: string, path: string) => {
    const options = {
        width: 100,
        height: 100,
        text: email,
        bgColor: '#' + materialColors[materialColors.length * Math.random() | 0],
        fontColor: '#FFFFFF',
        fontSize: 60
    }

    let username = email.substr(0, email.indexOf('@'));
    let text = username;
    if (username.length > 2) {
        text = username.substr(0, 2);
    }

    text = text.toLocaleUpperCase();

    gm(100, 100, options.bgColor)
        .fill(options.fontColor)
        .font(font)
        .drawText(0, 0, text, 'Center')
        .fontSize(options.fontSize)
        .setFormat('jpg')
        .write(path, (err) => {
            if (err) {
                console.log(err)
            }
        });
}

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    async createUser(userDto: UserDto, roleRepository: RoleRepository, mailService: MailService, userImgPath: string): Promise<string> {
        const { email, password } = userDto

        const user = new User()
        user.email = email;
        user.salt = await bcrypt.genSalt();
        user.password = await this.hashPassword(password, user.salt);
        user.role = await roleRepository.findOne({ where: { name: 'admin' } })
        user.imagePath = userImgPath;

        try {
            await user.save()
        } catch (error) {
            if (error.code === "23505") {
                throw new ConflictException(`An account associated with this email ${email} already exists`);
            } else {
                throw new InternalServerErrorException();
            }
        }

        // Send confirmation email

        const secret = uuidv4() + await bcrypt.genSalt();
        let queryParams = '?token=' + secret + '&email=' + email;
        mailService.sendConfirmationEmail(userDto.email, queryParams);

        createImage(user.email, userImgPath);
        return secret;
    }

    async findUserRelations(email: string): Promise<User> {
        return await this.findOne({ email }, { relations: ["role", "role.permissions", "organization"] });
    }

    async validateUserPassword(userLoginDto: UserLoginDto): Promise<User> {
        const { email, password } = userLoginDto;
        const user = await this.findUserRelations(email);
        const userPassword = await this.findOne({ email }, { select: ["password", "salt"] });
        if (user && await userPassword.validatePassword(password)) {
            return user;
        }

        return null;
    }

    async hashPassword(password: string, salt: string): Promise<string> {
        return bcrypt.hash(password, salt);
    }

}