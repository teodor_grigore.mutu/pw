import { Confirmation } from "src/entities/conf.entity";
import { EntityRepository, Repository } from "typeorm";

@EntityRepository(Confirmation)
export class ConfirmationRepository extends Repository<Confirmation> {

    createConfirmationEntry(email: string, secret: string) {
        let confirmation = new Confirmation();
        confirmation.email = email;
        confirmation.secret = secret
        let date = new Date()

        confirmation.expirationDate = new Date(date.getTime() + 15 * 60000);

        confirmation.save()
    }
}