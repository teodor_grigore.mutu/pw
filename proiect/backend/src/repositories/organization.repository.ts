import { Organization } from "src/entities/organization.entity";
import { EntityRepository, Repository } from "typeorm";

@EntityRepository(Organization)
export class OrganizationRepository extends Repository<Organization> { }