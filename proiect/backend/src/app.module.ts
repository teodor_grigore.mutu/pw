import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from 'src/config/typeorm.config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { OrganizationController } from './organization/organization.controller';
import { OrganizationService } from './organization/organization.service';
import { UserRepository } from './repositories/user.repository';
import { RoleRepository } from './repositories/role.repository';
import { ConfirmationRepository } from './repositories/confirmation.repository';
import { OrganizationRepository } from './repositories/organization.repository';
import { RoleController } from './role/role.controller';
import { RoleService } from './role/role.service';
import { PermissionRepository } from './repositories/permission.repository';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    TypeOrmModule.forFeature([
      UserRepository,
      RoleRepository,
      ConfirmationRepository,
      OrganizationRepository,
      PermissionRepository,
    ]),
    UserModule,
    HttpModule,
  ],
  controllers: [AppController, OrganizationController, RoleController],
  providers: [AppService, OrganizationService, RoleService],
})
export class AppModule { }
