import { BadRequestException, ForbiddenException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { ConfirmationDto, RefreshDto, UpdateUserDto, UserDto, UserLoginDto } from 'src/dto/user.dto';
import { User } from 'src/entities/user.entity';
import { ConfirmationRepository } from 'src/repositories/confirmation.repository';
import { RoleRepository } from 'src/repositories/role.repository';
import { createImage, UserRepository } from 'src/repositories/user.repository';
import { MailService } from 'src/services/mail/mail.service';
import { v4 as uuidv4 } from 'uuid';
import * as bcrypt from "bcrypt";
import { JwtPayload } from './jwt.payload';
import * as path from 'path';
import { CreateUserDto } from 'src/dto/role.dto';

const saveImgPath = path.join('/tmp', '/avatars/');

const randomStringGenerator = (length) => {
    var result = [];
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
}
@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserRepository)
        private userRepository: UserRepository,
        private roleRepository: RoleRepository,
        private confirmationRepository: ConfirmationRepository,
        private jwtService: JwtService,
        private mailService: MailService
    ) { }


    async deleteUser(id: number) {
        const result = await this.userRepository.delete(id);
        if (result.affected == 0) {
            throw new NotFoundException(`User with id ${id} was not found`);
        }
    }

    async getUserById(id: number): Promise<User> {
        const user = await this.userRepository.findOne(id);

        if (!user) {
            throw new NotFoundException(`User with ID "${id}" not found`);
        }

        return user;
    }

    async getUsers() {
        return await this.userRepository.find({ relations: ["role"] });
    }

    async register(userDto: UserDto) {
        let userImgPath = saveImgPath + randomStringGenerator(10) + '.jpg';
        let secret = await this.userRepository.createUser(userDto, this.roleRepository, this.mailService, userImgPath);
        await this.confirmationRepository.createConfirmationEntry(userDto.email, secret);
        return { "message": "Account created successfully. Confirmation email was sent it will expire in 15 minutes!" };
    }

    private async createJwt(user: User): Promise<{ accessToken: string, refreshToken: string }> {
        const payload = new JwtPayload(user);
        const accessToken = await this.jwtService.signAsync(Object.assign({}, payload));
        const refreshToken = await this.jwtService.signAsync(Object.assign({}, payload), { expiresIn: "1 years" });

        return { accessToken, refreshToken }
    }

    async auth(userLogintDto: UserLoginDto): Promise<{ accessToken: string, refreshToken: string }> {
        const user = await this.userRepository.validateUserPassword(userLogintDto);
        if (!user) {
            throw new UnauthorizedException('Invalid credentials');
        }
        if (!user.isVerified) {
            throw new UnauthorizedException(`Account with email ${user.email} is not verified`);
        }

        return await this.createJwt(user);
    }

    async confirm(confirmationDto: ConfirmationDto): Promise<{}> {
        let conf = await this.confirmationRepository.findOne({ where: { secret: confirmationDto.token } });

        let user = await this.userRepository.findOne({ where: { email: confirmationDto.email } });

        if (user.isVerified) {
            throw new BadRequestException(`User with email ${user.email} is verified`);
        }

        if (!conf) {
            throw new BadRequestException('Invalid secret key, no user associated with that key');
        }

        let currentDate = new Date();

        if (conf.expirationDate < currentDate) {
            throw new BadRequestException('Confirmation token expired');
        }

        user.isVerified = true;

        await user.save();
        await conf.remove();

        return { "message": "Account activated log in now!" };
    }

    async resend(confirmationDto: ConfirmationDto): Promise<{}> {

        let conf = await this.confirmationRepository.findOne({ where: { secret: confirmationDto.token } });

        if (!conf) {
            throw new BadRequestException('Invalid secret key, no user associated with that key');
        }

        let user = await this.userRepository.findOne({ where: { email: confirmationDto.email } });
        if (!user) {
            throw new BadRequestException(`No user associated with ${confirmationDto.email}`);
        }

        if (user.isVerified) {
            throw new BadRequestException(`User with email ${user.email} is verified`);
        }

        const secret = uuidv4() + await bcrypt.genSalt();
        let queryParams = '?token=' + secret + '&email=' + user.email;
        this.mailService.sendConfirmationEmail(user.email, queryParams);

        await conf.remove();
        await this.confirmationRepository.createConfirmationEntry(user.email, secret);

        return { "message": "Resend account confirmation!" };
    }

    async refresh(refreshDto: RefreshDto) {
        try {
            await this.jwtService.verifyAsync(refreshDto.refreshToken);
        }
        catch (errror) {
            throw new UnauthorizedException();
        }

        const jwt = await this.jwtService.decode(refreshDto.refreshToken);

        const user = await this.userRepository.findUserRelations(jwt['email']);

        return this.createJwt(user);
    }

    async createUser(createUserDto: CreateUserDto, user: User) {
        let userDb = await this.userRepository.findUserRelations(createUserDto.email);
        
        if (userDb) {
            throw new BadRequestException(`Email ${userDb.email} in use`);
        }

        let role = await this.roleRepository.findOne({id: createUserDto.roleId});
        if (!role) {
            throw new NotFoundException(`Role not found`);
        }

        let newUser = new User();
        newUser.email = createUserDto.email;
        newUser.role = role;
        newUser.password = uuidv4();
        const passwordCopy = newUser.password
        newUser.isVerified = true;
        newUser.salt = await bcrypt.genSalt();
        newUser.password = await bcrypt.hash(newUser.password, newUser.salt);
        newUser.organization = user.organization;
        newUser.imagePath = saveImgPath + randomStringGenerator(10) + '.jpg';
        createImage(newUser.email, newUser.imagePath);

        this.mailService.createUserAcoount(newUser.email, passwordCopy);

        await newUser.save();
    }

    async update(id: number, updateUserDto: UpdateUserDto, user: User) {
        let userDb = await this.userRepository.findOne( { id: id }, { relations: ['role']});
        if (!userDb) {
            throw new NotFoundException(`Cannot find user`);
        }

        let role = await this.roleRepository.findOne({ id: updateUserDto.roleId });
        if (!role) {
            throw new NotFoundException(`Cannot find role`);
        }
    
        userDb.role = role;
        await userDb.save();
    }
}
