import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { User } from 'src/entities/user.entity';

@Injectable()
export class PermissionGuard implements CanActivate {
    constructor(private reflector: Reflector) { }

    canActivate(context: ExecutionContext): boolean {
        const permission = this.reflector.get<string[]>('permission', context.getHandler());
        const request = context.switchToHttp().getRequest();
        const user: User = request.user;

        if (!user.role.permissions.find(p => p.permission === permission[0])) {
            throw new UnauthorizedException(`Insufficient permission`);
        }

        return true;
    }
}