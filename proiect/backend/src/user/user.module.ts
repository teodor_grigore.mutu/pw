require('dotenv').config()
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/repositories/user.repository';
import { RoleRepository } from 'src/repositories/role.repository';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MailService } from 'src/services/mail/mail.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { ConfirmationRepository } from 'src/repositories/confirmation.repository';
import { JwtStragety } from './jwt.strategy';
import * as path from 'path';
@Module({
    imports: [
        PassportModule.register({
            defaultStrategy: "jwt"
        }),
        JwtModule.register({
            secret: process.env.JWT_SECRET,
            signOptions: {
                expiresIn: 3600,

            },
        }),
        TypeOrmModule.forFeature([
            UserRepository,
            RoleRepository,
            ConfirmationRepository
        ]),
        MailerModule.forRoot({
            transport: {
                host: process.env.EMAIL_HOST,
                port: process.env.EMAIL_PORT,
                secure: true,
                auth: {
                    user: process.env.EMAIL_ID,
                    pass: process.env.EMAIL_PASS,
                },
            },
            defaults: {
                from: process.env.EMAIL_ID,
            },
            template: {
                dir: path.join(__dirname, '/../../../templates'),
                adapter: new HandlebarsAdapter(),
                options: {
                    strict: true,
                },
            },
        })
    ],
    controllers: [UserController],
    providers: [
        UserService,
        MailService,
        JwtStragety,
    ],
    exports: [
        JwtStragety,
        PassportModule,
    ]
})
export class UserModule { }
