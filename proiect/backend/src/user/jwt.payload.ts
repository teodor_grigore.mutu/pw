import { Role } from "src/entities/role.entity";
import { User } from "src/entities/user.entity";

export class JwtPayload {
    email: string;

    role: Role;

    constructor(user: User) {
        this.email = user.email;
        this.role = user.role
    }
}