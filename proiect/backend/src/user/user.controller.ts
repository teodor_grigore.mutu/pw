import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Res, SetMetadata, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { of } from 'rxjs';
import { CreateUserDto } from 'src/dto/role.dto';
import { ConfirmationDto, RefreshDto, UpdateUserDto, UserDto, UserLoginDto } from 'src/dto/user.dto';
import { User } from 'src/entities/user.entity';
import { GetUser } from './get-user.decorator';
import { PermissionGuard } from './permission.guard';
import { UserService } from './user.service';

@Controller('user')
export class UserController {

    constructor(
        private userService: UserService
    ) { }


    @Post('/register')
    signUp(@Body(ValidationPipe) userDto: UserDto) {
        return this.userService.register(userDto);
    }

    @Post('/auth')
    signIn(@Body(ValidationPipe) userLogintDto: UserLoginDto): Promise<{ accessToken: string, refreshToken: string }> {
        return this.userService.auth(userLogintDto);
    }

    @Post('/confirmation')
    confirmation(@Body(ValidationPipe) confirmationDto: ConfirmationDto): Promise<{}> {
        return this.userService.confirm(confirmationDto);
    }

    @Post('/resend')
    resendConfirmation(@Body(ValidationPipe) confirmationDto: ConfirmationDto): Promise<{}> {
        return this.userService.resend(confirmationDto);
    }


    @Post('/refresh')
    refreshToken(@Body(ValidationPipe) refreshDto: RefreshDto) {
        return this.userService.refresh(refreshDto);
    }

    @Post('/create')
    @SetMetadata('permission', ['create-user'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    createUser(@GetUser() user: User, @Body(ValidationPipe) createUserDto: CreateUserDto) {
        return this.userService.createUser(createUserDto, user);
    }

    @Get('')
    @SetMetadata('permission', ['list-user'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    getUsers() {
        return this.userService.getUsers()
    }

    @Get('/profile-image')
    @UseGuards(AuthGuard())
    findProfileImage(@GetUser() user: User, @Res() res) {
        return of(res.sendFile(user.imagePath));
    }

    @Delete(':id')
    @Get('/test/fail')
    @SetMetadata('permission', ['delete-user'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    deleteUser(@Param('id', ParseIntPipe) id: number) {
        return this.userService.deleteUser(id)
    }

    @Put(':id')
    @SetMetadata('permission', ['modify-user'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    updateUser(@GetUser() user: User, @Param('id', ParseIntPipe) id: number, @Body(ValidationPipe) updateUserDto: UpdateUserDto) {
        return this.userService.update(id, updateUserDto, user);
    }

    @SetMetadata('permission', ['modifyyhkhkhkhkjhkhkky-user'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    testBad() {

    }

    @Get('/test/success')
    @SetMetadata('permission', ['modify-user'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    testGood() {
        return { ok: "ok" };
    }
}
