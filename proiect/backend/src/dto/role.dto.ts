import { ArrayUnique, IsArray, IsEmail, IsNumber, IsString } from "class-validator";


export class RoleDto {
    @IsString()
    name: string;

    @IsArray()
    @ArrayUnique()
    permissions: string[];
}

export class UpdateRoleDto extends RoleDto {
    @IsNumber()
    id: number;
}

export class CreateUserDto {
    @IsNumber()
    roleId: number;

    @IsString()
    @IsEmail()
    email: string;
}