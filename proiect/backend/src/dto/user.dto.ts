import { IsEmail, IsNumber, IsString, Matches, MaxLength, MinLength } from "class-validator";

export class UserDto {
    @IsEmail()
    email: string;

    @MinLength(8)
    @MaxLength(76)
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, { message: 'Password too weak' })
    password: string;
}

export class UserLoginDto {
    @IsEmail()
    email: string;

    password: string;
}
export class ConfirmationDto {
    @IsString()
    email: string;

    @IsString()
    token: string;
}

export class RefreshDto {
    @IsString()
    refreshToken: string;
}

export class UpdateUserDto {
    @IsNumber()
    roleId: number;
}