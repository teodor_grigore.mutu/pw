import { BaseEntity, Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm";

@Entity()
@Unique("uq_permission", ["permission"])
export class Permission extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'text', nullable: false })
    permission: string;


    constructor(permission: string) {
        super();

        this.permission = permission;
    }
}