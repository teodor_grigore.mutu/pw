import { BaseEntity, Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import { Permission } from "./permission.entity";
import { User } from "./user.entity";

@Entity()
@Unique("uq_role_name", ["name"])
export class Role extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToMany(() => Permission)
    @JoinTable()
    permissions: Permission[];

    @OneToMany(() => User, user => user.role)
    users: User[];

    constructor(name: string, permissions: Permission[]) {
        super();

        this.name = name;
        this.permissions = permissions
    }

}