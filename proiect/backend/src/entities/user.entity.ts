import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn, Unique } from "typeorm";
import * as bcrypt from "bcrypt";
import { Role } from "./role.entity";
import { Organization } from "./organization.entity";

@Entity()
@Unique("uq_user_email", ["email"])
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "text", nullable: false })
    email: string;

    @Column({ type: "text", select: false, nullable: false })
    password: string;

    @Column({ type: "text", select: false, nullable: false })
    salt: string;

    @Column({ type: "bool", default: false })
    isVerified: boolean;

    @Column({ type: "text", nullable: false})
    imagePath: string;

    @ManyToOne(() => Role, role => role.users)
    role: Role;

    @ManyToOne(() => Organization, organization => organization.users, { createForeignKeyConstraints: false })
    organization: Organization;

    async validatePassword(password: string): Promise<boolean> {
        const hash = await bcrypt.hash(password, this.salt);
        return hash === this.password;
    }

}