import { Entity, BaseEntity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Confirmation extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "text" })
    email: string;

    @Column({ type: 'text', nullable: false })
    secret: string;

    @Column({ type: "timestamp", nullable: false })
    expirationDate: Date;
}