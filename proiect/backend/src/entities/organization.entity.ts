import { Entity, BaseEntity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Organization extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: string;

    @Column({ type: "text" })
    name: string;

    @OneToMany(() => User, user => user.organization, { createForeignKeyConstraints: false })
    users: User[]
}