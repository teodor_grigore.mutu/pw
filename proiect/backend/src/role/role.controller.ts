import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, SetMetadata, UseGuards, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RoleDto, UpdateRoleDto } from 'src/dto/role.dto';
import { PermissionGuard } from 'src/user/permission.guard';
import { RoleService } from './role.service';

@Controller('role')
export class RoleController {

    constructor(
        private roleService: RoleService
    ) { }

    @Post('/create')
    @SetMetadata('permission', ['create-role'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    createRole(@Body(ValidationPipe) roleDto: RoleDto) {
        return this.roleService.createRole(roleDto);
    }

    @Get('')
    @SetMetadata('permission', ['list-role'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    getRoles() {
        return this.roleService.getRoles();
    }

    @Delete('/:id')
    @SetMetadata('permission', ['delete-role'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    deleteRole(@Param('id', ParseIntPipe) id: number) {
        return this.roleService.deleteRole(id);
    }

    @Put('/:id')
    @SetMetadata('permission', ['modify-role'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    updateRole(@Param('id', ParseIntPipe) id: number, @Body(ValidationPipe) updatedRole: UpdateRoleDto) {
        return this.roleService.updateRole(updatedRole);
    }
}
