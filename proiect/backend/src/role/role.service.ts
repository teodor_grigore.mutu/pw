import { BadRequestException, ConflictException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { RoleDto, UpdateRoleDto } from 'src/dto/role.dto';
import { Permission } from 'src/entities/permission.entity';
import { Role } from 'src/entities/role.entity';
import { userRolePermission1618491908655 } from 'src/migration/1618491908655-user_role_permission';
import { PermissionRepository } from 'src/repositories/permission.repository';
import { RoleRepository } from 'src/repositories/role.repository';

@Injectable()
export class RoleService {

    private permission_names: string[] = [
        "create-user", "delete-user", "list-user", "modify-user",
        "create-role", "list-role", "delete-role", "modify-role",
        "create-organization", "delete-organization", "modify-organization", "list-organization",
        "create-resource", "delete-resource", "list-resource", "modify-resource"
    ];

    constructor(private roleRepository:RoleRepository, private permissionRepository: PermissionRepository) { }

    async createRole(roleDto: RoleDto) {

        let permissions: Permission[] = [];
        for (let permission of roleDto.permissions) {
            if (!this.permission_names.includes(permission)) {
                throw new BadRequestException(`Invalid permission ${permission}!`);
            }

            let dbPermission = await this.permissionRepository.findOne({permission: permission});
            permissions.push(dbPermission);
        }

        let role = new Role(roleDto.name, permissions);
        role.name = roleDto.name;

        try {
            await role.save();
        } catch (error) {
            if (error.code === "23505") {
                throw new ConflictException(`A role with this name ${role.name} already exists`);
            } else {
                throw new InternalServerErrorException();
            }
        }
    }

    async getRoles() {
        return await this.roleRepository.findRolesRelations();
    }

    async deleteRole(id: number) {
        const result = await this.roleRepository.delete(id);
        if (result.affected == 0) {
            throw new NotFoundException(`Role with id ${id} was not found`);
        }
    }

    async updateRole(updateRoleDto: UpdateRoleDto) {
        let role = await this.roleRepository.findOne({id: updateRoleDto.id}, {relations: ['permissions']})
        if (!role) {
            throw new NotFoundException(`Role with id ${updateRoleDto.id} was not found`);
        }

        for (let permission of updateRoleDto.permissions) {
            if (!this.permission_names.includes(permission)) {
                throw new BadRequestException(`Invalid permission ${permission}!`);
            }

            const index = role.permissions.findIndex(p => p.permission === permission);
            if (index <= -1) {
                const permissionDb = await this.permissionRepository.findOne({permission: permission});
                role.permissions.push(permissionDb);
            }
        }

        for (let permission of role.permissions) {
            const index = updateRoleDto.permissions.findIndex(p => p === permission.permission)
            if (index <= -1) {
                const permissionIndex = role.permissions.indexOf(permission);
                role.permissions.splice(permissionIndex, 1); 
            }
        }

        return await role.save();
    }
}
