import { BadRequestException, Injectable } from '@nestjs/common';
import { OrganizationDto } from 'src/dto/organization.dto';
import { Organization } from 'src/entities/organization.entity';
import { User } from 'src/entities/user.entity';
import { OrganizationRepository } from 'src/repositories/organization.repository';
import { UserRepository } from 'src/repositories/user.repository';

@Injectable()
export class OrganizationService {

    constructor(private organizationRepository: OrganizationRepository, private userRepository: UserRepository) { }


    async createOrganization(organizationDto: OrganizationDto, user: User) {
        const organization = new Organization();
        organization.name = organizationDto.name;

        if (user.organization) {
            throw new BadRequestException(`You cannot have more than 1 organization!`);
        }

        await organization.save();
        user.organization = organization;
        await user.save();
    }

}
