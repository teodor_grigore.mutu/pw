import { Body, Controller, Get, Post, SetMetadata, UseGuards, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { OrganizationDto } from 'src/dto/organization.dto';
import { User } from 'src/entities/user.entity';
import { GetUser } from 'src/user/get-user.decorator';
import { PermissionGuard } from 'src/user/permission.guard';
import { OrganizationService } from './organization.service';

@Controller('organization')
export class OrganizationController {

    constructor(private organizationService: OrganizationService) { }

    @Post('/create')
    @SetMetadata('permission', ['create-organization'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    createOrganization(@GetUser() user: User, @Body(ValidationPipe) organizationDto: OrganizationDto) {
        return this.organizationService.createOrganization(organizationDto, user)
    }

    @Get('/name')
    @SetMetadata('permission', ['list-organization'])
    @UseGuards(PermissionGuard)
    @UseGuards(AuthGuard())
    getOrganization(@GetUser() user: User){
        if (user.organization) {
            return {name: user.organization.name};
        }

        return {name: undefined};
    }

}
