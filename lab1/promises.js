console.log("Before promise");

const promise = new Promise((resolve, reject) => {
    setTimeout(_ => {
        console.log("after 2 seconds");
        resolve();
    }, 2000);
});

promise.then(_ => {
    console.log("done");
});
console.log("Log after promise");