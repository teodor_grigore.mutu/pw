// Task 1

console.log('Hello Word');

// Task 2

const date = new Date();

console.log(`date: ${date.getDate()} hours: ${date.getHours()}`);

// Task 3

const array = Array.from(new Array(100), (x, i) => i);

console.log(array.filter(number => number % 2 === 0).join(', '));

// Task 4

let func = (array, index, _lambda = (x) => { console.log(`element is ${x}`) }) => {
    _lambda(array[index])
};

func(array, 19);

func(array, 2, (x) => { console.log(`double of the element ${x} will be ${x * 2}`) });

// Task 5
// Async await

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    })
};
console.log("Before asyncwrapper");
const asyncWrapper = async () => {

    await sleep(2000);
    console.log("after await");
}
asyncWrapper();