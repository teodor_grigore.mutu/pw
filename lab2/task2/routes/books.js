const router = require('express').Router()
const books = require('./../database')

router.get('/', (req, res) => {

    let author = req.query.author;
    if (author) {
        return res.send(books.getFromDbByAuthor()).status(200);
    }

    return res.send(books.getAllFromDb()).status(200);
});

router.get('/:id', (req, res) => {
    let id = req.params.id

    try {
        return res.send(books.getFromDbById(id)).status(200);
    }
    catch (exc) {
        return res.send({ Error: exc.message }).status(404);
    }
});

router.post('/', (req, res) => {
    books.insertIntoDb(req.body);

    return res.send({ SuccessMessage: 'Book created.' }).status(201);
});

router.delete('/', (req, res) => {

    let author = req.query.author;
    if (author) {
        books.removeFromDbByAuthor(author);
    } else {
        books.purgeDb();
    }

    return res.send({ SuccessMessage: 'Books deleted.' }).status(204);
});

router.delete('/:id', (req, res) => {
    let id = req.params.id

    books.removeFromDbById(id);
    return res.send({ SuccessMessage: 'Book deleted.' }).status(204);
});

router.put('/:id', (req, res) => {
    let id = req.params.id

    if (id != req.body.id) {
        return res.send({ Error: "Id doesn't match" }).status(400);
    }

    try {
        books.updateById(id, req.body)
        return res.send({ SuccessMessage: 'Book updated.' }).status(200);
    } catch (exc) {
        return res.send({ Error: exc.message }).status(404);
    }

});

module.exports = router;
