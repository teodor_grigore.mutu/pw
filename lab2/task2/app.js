const express = require('express');
const moment = require('moment');
const routes = require('./routes/routes')

const app = express();

app.use(express.json());

app.get('/', (req, res) => {
    res.json(moment());
});

app.use(routes);

app.listen(3000, () => {
    console.log(`Server started 3000`)
});