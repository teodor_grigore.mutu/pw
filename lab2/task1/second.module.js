const { sum } = require('./first.module');

function sameParity(array, parity) {
    if (!parity) {
        throw new Error(`Parity cannot be ${parity}`);
    }
    const newParity = parity % 2;
    return sum(array.filter(elem => elem % newParity == 0));
}

module.exports = {
    sameParity
}