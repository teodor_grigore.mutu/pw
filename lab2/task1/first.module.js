

function sum(array) {
    return array.reduce((a, b) => a + b, 0);
}

module.exports = {
    sum
}
